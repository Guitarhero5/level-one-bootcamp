#include<stdio.h>
#include<math.h>

float input()
{
    float a;
    scanf("%f", &a);
    return a;
}

float distance(float X1, float X2, float Y1, float Y2)
{
    float D = sqrt((X2-X1)*(X2-X1)+(Y2-Y1)*(Y2-Y1));
    return D;
}

float output(float a)
{
    printf("The distance is %f", a);
    return a;
}

void main()
{
    float X1, X2, Y1, Y2, D;
    
    printf("Enter abscissa of the first point: ");
    X1 = input();
    
    printf("Enter abscissa of the second point: ");
    X2 = input();
    
    printf("Enter ordinate of first point: ");
    Y1 = input();
    
	printf("Enter ordinate of second point: ");
    Y2 = input();
	
    D = distance(X1,X2,Y1,Y2);
    
    output(D);
}