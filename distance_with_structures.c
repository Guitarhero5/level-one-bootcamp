#include<stdio.h>
#include<math.h>

struct Points
{
    float x, y;
};

float distance(struct Points m, struct Points n)
{
    float result;
    result = sqrt((m.x - n.x) * (m.x - n.x) + (m.y - n.y) *(m.y - n.y));
    return result;
}

int main()
{
    struct Points m, n;

    printf("\n Details of Point 1:\n");
    printf("\n X-intercept: \t");
    scanf("%f", &m.x);
    printf("\n Y-intercept: \t");
    scanf("%f", &m.y);

    printf("\n Details of Point 2:\n");
    printf("\n X-intercept:\t");
    scanf("%f", &n.x);
    printf("\n Y-intercept: \t");
    scanf("%f", &n.y);

    printf("\n Distance between Points A and B: %f\n", distance(m, n));
    return 0;
}
